

// radix value
var BASE 					= 100000;
// how many symbols would be used to present single digit
var SYM_PER_DIGIT			=((BASE-1)+"").length;
// unnecessary extension for String type with padd method, which is default method for now
var PADDING_MASK			="000000000000000000000";
var PADDING_VALUE			=PADDING_MASK.substring(0,SYM_PER_DIGIT);
String.prototype.paddingLeft 	=function(paddingValue){return String(paddingValue + this).slice(-paddingValue.length)};

// max combinetion to present single digit 
var MAX_DIGIT_VALUE			=BASE-1;
// quantity of digits in single LongInt
var LONG_LEN				=12;

// make random value of digit by function call 
var RND_DIGIT = function(){return Math.floor(Math.random()*(MAX_DIGIT_VALUE-1)+1)};
// make array of [num] random digits by function call 
var RND_ARRAY = function(num){return Array.apply(null, {length: num}).map(Function.call, RND_DIGIT)};
// make array of [num] zerro digits by function call 
var ZRO_ARRAY = function(num){return Array.apply(null, {length: num}).map(Function.call, function(){return 0})};


/*

Special class to store result of operation over pair of digits in standart data type number

*/

class TBuffer {

	constructor(x) {
		x = x || [];
		this.valData = x.length > 0 ? x.reduce(function (prev, cur) {return prev + cur}) : 0;
		this.base = BASE;
	};


	rem(){
			var r = this.valData % this.base;
			this.shr();
			return r
		};
		
	shr(){this.valData = Math.floor(this.valData / this.base)};

	isZero(){
			if (this.valData == 0) {
				return true
			} else {
				return false
			}
		};

	notEmpty(){return !this.isZero()};


	PushSum(x){
			if (x.length > 1) {
				this.valData += x.reduce(function (prev, cur) {
					return prev + cur
				})
			} else {
				this.valData += x
			}
		};

	PushProd(x) {
			if (x.length > 1) {
				this.valData += x.reduce(function (prev, cur) {
					return prev * cur
				})
			} else {
				this.valData += x
			}
		};
}


/*
This is a JavaScript class that represents a number. 
It has properties such as digits, negative, and layout, and methods such as first(), last(), next(), prev(), is_leading_zerro(), isFull(), setRand(), currentAdd(), lsd(), digits(), and snapshoot(). 
These methods allow the user to 
			navigate through the digits of the number, 
			check whether the current digit is a leading zero or the number is full, 
			set the digits of the number to a random array of numbers, 
			add a number to the current digit of the number, 
			get or set the least significant digit and digits of the number, 
			and update the HTML layout of the number.
*/

// This is a class called TLNum, which represents a number.
// The constructor function takes an object as a parameter, which may have the following properties:
// - digits: an array of numbers representing the digits of the number (default is an array of zeros)
// - n: a boolean indicating whether the number is negative (default is false)
// - layout: an object representing the layout of the number in the HTML document (default is null)
// The properties of the object are then assigned to the class instance using the "this" keyword.
/*
	str() method returns a string representation of the number, with leading zeros removed. 
			It uses the reduceRight method to concatenate the digits of the number into a string, and adds left padding to each digit using a constant PADDING_VALUE.
	getLayout() method returns an HTML layout of the number, with the current number value and the current least significant digit displayed. 
			It uses the str() and digits() methods to get the number value and digits array, respectively, and concatenates them into an HTML string.
	currentVal(new_val) method gets or sets the current digit of the number. 
			If called without an argument, it returns the current digit. If called with an argument, it sets the current digit to the new value, as long as the number is not full.
	setDigits(digit_array) method sets the digits of the number to a given array of numbers. 
			It starts from the first digit and fills in the array until the number is full, or until the end of the array. It then sets the least significant digit to the current digit.
	addShort(buffer_short) method adds a short TLNum (with fewer digits) to the current TLNum. 
			It uses a buffer to keep track of the carry-over digits, and updates the least significant digit of the number if necessary.
	add(LNum) method adds another TLNum to the current TLNum. 
			It starts by adding the least significant digits of both numbers and the carry-over digit to a buffer. It then iterates through the digits of both numbers, adding the digits and the carry-over digit to the buffer, and updating the current digit and the least significant digit of the number accordingly.
	mul(LNum) method multiplies another TLNum to the current TLNum. 
			It starts by creating a new TLNum to store the result, and a buffer to keep track of the carry-over digits. It then iterates through the digits of both numbers, multiplying each digit with the current digit of the other number, and adding the result and the carry-over digit to the buffer. It updates the current digit and the least significant digit of the result TLNum accordingly.

*/

class TLNum{
	constructor(s){
		s = s||{};
		this._digits 			=s['digits']||ZRO_ARRAY(LONG_LEN);
		this.maxlen				=s['digits'] ? s['digits'].length:LONG_LEN;
		this.negative			=s['n']||false;
		this.layout				=s['layout']||null;
		this.how				=[];
		this.ref				=0;
		this.rem=0;
		this._current			=0;
		this.lsd_num			=0;
		// This function sets the least significant digit of the number.
		// It is called when the number is first created or when its digits change.
		this.auto_len()
		return this;
	};

	// These functions allow the user to navigate through the digits of the number.
	first(){this._current=0; return this.currentVal()};
	last(){this._current=this.lsd_num; return this.currentVal()};
	next(){this._current+=1; return this.currentVal()};
	prev(){this._current-=1; return this.currentVal()};

	// This function checks whether the current digit is a leading zero.
	is_leading_zerro(){return this._current > this.lsd_num};

	// This function checks whether the number is full (i.e. all digits have been set).
	isFull(){return (this._current == this._digits.length )};

	// This function sets the least significant digit of the number.
	auto_len(){this.lsd_num = this._digits.reduce(function(p,c,i){if(c!=0)  p=i; return p },0)};

	// This function sets the digits of the number to a random array of numbers.
	setRand(dig_to_set){this._digits.fill(0); this.setDigits(RND_ARRAY(dig_to_set)); return this };

	// This function adds a number to the current digit of the number.
	currentAdd(el_to_add){this._digits[this._current]+=el_to_add};

	// These functions allow the user to get or set the least significant digit and digits of the number.
	lsd(new_num){if (new_num==undefined) {return this.lsd_num} else  this.lsd_num=new_num};
	digits(array_of_digit){if (array_of_digit==undefined) {return this._digits} else this.setDigits(array_of_digit)};

	// This function updates the HTML layout of the number.
	snapshoot(){this.layout.html(this.getLayout()); return this;}

/*
		Each of these methods performs a specific task in the LNum class.
*/

/*		The str() method returns a string representing a number formatted with the paddingLeft function. 
		This is done by slicing the _digits array from 0 to lsd_num + 1, where lsd_num is the index of the least significant digit in the number. 
		The reduceRight method is then used to concatenate the digits into a string, using the paddingLeft function to format each digit before concatenating.
*/		
	

		str(){
												
													var st=this._digits.slice(0,this.lsd_num+1)
																		.reduceRight(function(prev,cur){
																					return prev + cur.toString().paddingLeft(PADDING_VALUE)
																		},"");
				var s=st.split("")
			
			while (s[0]=="0"){s.shift()}
			st=s.join('');
			return this.negative?`-${st}`:st;
										}
/*		
		The getLayout() method returns an HTML string with two containers. 
		The first container contains the string representation of the number that is obtained by calling the str() method. 
		The second container contains an array of the number's digits and its least significant digit index.
*/		
		getLayout(){
								var l = '<span class="container">'+this.str()+'</span>'
							+'<span class="container">'+'['+this.digits().join(',')+']'+' lsd = '+this.lsd()+'</span>';
								return l;
							}
/*		
		The currentVal(new_val) method allows you to get or set the value of the current digit. 
		If new_val is not defined, then the method returns the value of the current digit. 
		If new_val is defined, then the method sets the value of the new digit and returns true if the digit was successfully set. 
		If the current digit is the least significant digit, then the method returns false.
*/		
		currentVal(new_val){
											if (new_val==undefined) {return (this._digits[this._current] )}
											else  {
														if(this._current==this.maxlen) 	{return false;}
														this._digits[this._current]=new_val; return true
											}
										};
/*		
		The setDigits(digit_array) method sets an array of digits in a number. 
		The method resets the value of each digit to 0, then sets the digits according to the digit_array using the currentVal(), next() and isFull() methods. 
		The least significant digit is also set to the last digit set.
*/		
		
		setDigits(digit_array) {
											this.first(); this._digits.fill(0);
											while (!this.isFull() && this._current<digit_array.length) {
												this.currentVal(digit_array[this._current]);
												this.next()
											}
											if (!this.isFull()) {this.prev();}
											this.lsd(this._current)
										}
/*		
		The addShort(buffer_short) method adds the short number present in buffer_short to a number.
		The method starts with the least significant digit and adds digits from the buffer using the PushSum(), currentVal(), next(), and isFull() methods. 
		The least significant digit is updated if the new digit is greater than its value.
*/	
		addShort(buffer_short) {
											buffer_short.PushSum(this.first());
											while(buffer_short.notEmpty()){
												this.currentVal(	buffer_short.rem() );
												buffer_short.PushSum(this.next());
											}
											if(this._current>this.lsd()) {this.lsd(_current);}
											this.first();
										}
		
/*		
		The add(LNum) method adds two numbers. 
		The method starts with the least significant digit and adds the digits of the two numbers using the currentVal(), next(), is_leading_zerro(), and PushSum() methods. 
		The method also updates the value of the least significant digit.
*/		
		add(LNum){
											this.ref= parseInt(this.str())+parseInt(LNum.str());
											var	buffer_short = new TBuffer([this.first(), LNum.first()]);
											while(!LNum.is_leading_zerro()||buffer_short.notEmpty()){
												if(this.currentVal( buffer_short.rem() )){
														buffer_short.PushSum([this.next(),	LNum.next()]);
												} else {console.log('ofl while doing add'); return false}
											}
											this.prev();  this.lsd(this.lsd()>this._current ? this.lsd():this._current);
											this.first(); LNum.first();
											return this;
										}

												
/*		
		The mul(LNum) method multiplies two numbers. 
		The method starts with the least significant digit and multiplies the digits of the two numbers using the currentVal(), next(), is_leading_zerro(), and PushSum() methods. 
		The result of the multiplication is represented in the res object. 
		The method also updates the value of the least significant digit.
*/
		mul(LNum){
											var res = new TLNum(),
											buffer_short = new TBuffer([res.currentVal(),this.first()*LNum.first()]);

								 			this.ref= parseInt(this.str())*parseInt(LNum.str());

								 			while(!LNum.is_leading_zerro()){
												while(!this.is_leading_zerro()||buffer_short.notEmpty()){
													if(res.currentVal( buffer_short.rem() )){
														buffer_short.PushSum([res.next(), this.next()*LNum.currentVal()]);
													} else {console.log('ofl while doing mul'); return false}
												}
												LNum.next(); res._current=LNum._current;
												buffer_short.PushSum([res.currentVal(), this.first()*LNum.currentVal()]);
											}
											this.digits(res.digits());
											this.auto_len(); this.first(); LNum.first();
											return this;
										}

										// Правки внесены в метод sub(LNum) класса TLNum для корректного выполнения операции вычитания.
 /*   
    sub(LNum){
        // Проверка, нужно ли инвертировать результат после вычитания
        if (this.lsd()<LNum.lsd() || (this.lsd()==LNum.lsd() && this._digits[this.lsd()]<LNum._digits[LNum.lsd()])) {
            let tempDigits = this.digits(); // Сохраняем текущие цифры
            this.digits(LNum.digits()); // Устанавливаем цифры вычитаемого числа
            LNum.digits(tempDigits); // Устанавливаем сохраненные цифры уменьшаемого числа
            this.negative = !this.negative; // Инвертируем знак результата
        }
        
        let buffer_short = new TBuffer([this.first(), BASE - LNum.first()]); // Инициализация буфера с учетом базы и первой цифры вычитаемого
        
        // Выполнение операции вычитания с заимствованием
        while(!LNum.is_leading_zerro() || buffer_short.notEmpty()){
            let currentVal = buffer_short.rem() - BASE; // Вычитаем базу для коррекции результата
            this.currentVal((currentVal >= 0) ? currentVal : currentVal + BASE); // Корректировка текущего значения
            
            if(currentVal < 0) {
                // Если результат отрицательный, нужно выполнить заимствование
                buffer_short.PushSum([-1, this.next(), BASE - LNum.next()]);
            } else {
                buffer_short.PushSum([this.next(), BASE - LNum.next()]);
            }
        }
        
        // Удаление ведущих нулей из результата
        this.last();
        while (this.currentVal() == 0 && this._current > 0) this.prev();
        this.lsd(this._current); // Обновляем индекс наименьшего значащего разряда
        this.first(); // Возвращаемся к началу числа
        LNum.first(); // Возвращаем указатель в начало у LNum
    }


}
*/

  sub(LNum){
        // Проверка, нужно ли инвертировать результат после вычитания
        if (this.lsd()<LNum.lsd() || (this.lsd()==LNum.lsd() && this._digits[this.lsd()]<LNum._digits[LNum.lsd()])) {
		    var tempDigits=new Object();
			Object.assign(tempDigits,LNum)
			Object.setPrototypeOf(tempDigits, LNum);
		    tempDigits.sub(this);
			Object.assign(this,tempDigits);
			this.negative=true;
			
			return this
			}
        
        let buffer_short = new TBuffer([this.first(), BASE - LNum.first()]); // Инициализация буфера с учетом базы и первой цифры вычитаемого
        
        // Выполнение операции вычитания с заимствованием
        while(!LNum.is_leading_zerro() && buffer_short.notEmpty()){
            let currentVal = buffer_short.rem() - BASE; // Вычитаем базу для коррекции результата
            this.currentVal((currentVal >= 0) ? currentVal : currentVal + BASE); // Корректировка текущего значения
            
            if(currentVal < 0) {
                // Если результат отрицательный, нужно выполнить заимствование
                buffer_short.PushSum([-1, this.next(), BASE - LNum.next()]);
            } else {
                buffer_short.PushSum([this.next(), BASE - LNum.next()]);
            }
        }
        
        // Удаление ведущих нулей из результата
        this.last();
        while (this.currentVal() == 0 && this._current > 0) this.prev();
        this.lsd(this._current); // Обновляем индекс наименьшего значащего разряда
        this.first(); // Возвращаемся к началу числа
        LNum.first(); // Возвращаем указатель в начало у LNum
		return this
    }



		compare(b){
			if(this.lsd()<b.lsd()){return -1}
			if(this.lsd()>b.lsd()){return 1}
			if(this.first()<b.first()){return -1}
			if(this.first()>b.first()){return 1}
			
			for(let i=0;i<this.lsd();i++){
				if(this.next()<b.next()){return -1}
				if(this.next()>b.next()){return 1}
			}
			return 0
		}


										
		bad_sub(LNum){
					if (this.compare(LNum)<0) {
						let a=this.digits().map(e=>e); this.digits(LNum.digits()); LNum.digits(a); this.negative=true;
					}
						var	buffer_short = new TBuffer([this.first(), BASE, -LNum.first()]);

					while(!LNum.is_leading_zerro()&&buffer_short.notEmpty()){
						this.currentVal(buffer_short.rem());
						buffer_short.PushSum([-1, this.next() , BASE , -LNum.next()]);
					}
			
				if(buffer_short.notEmpty()){
					this.next(); 
					this.currentVal(this.currentVal()+buffer_short.rem());
					
					//this.currentVal(this.currentVal()-buffer_short.rem())
				}
							this.last();
							while (this.currentVal()==0) this.prev();
							this.lsd(this._current); this.first(); LNum.first()
		return this				
		}



		// For convenience, let's make a small "auxiliary" method
// of multiplying the current TLNum by BASE by "shifting". You can do it via mul,
// but it comes out faster and clearer this way.
shiftBaseLeft() {
	// If the current number is 0 (lsd_num=0 and _digits[0]=0),
	// then there is nothing to shift:
			if (this.lsd() === 0 && this._digits[0] === 0) {
				return this;
			}
	// Check that there is a place in the array:
			if (this.lsd_num + 1 >= this._digits.length) {
				console.log("Overflow while shifting by base");
				return this;
			}
			// We shift all the digits "up". Because according to the condition
			// LSD (least significant digit) has a higher index (lsd_num),
			// and index 0 is the "left" end. But everything is upside down,
	// so it's easier to make your own loop/ or use unshift().
			// However, be careful, unshift(0) shifts everything "backwards",
			// and we need to match LSD = maxIndex. 
			// For clarity, let's do it manually.
	
			// Just add a new 0 to the "end" of the array and move lsd_num:
	this._digits.splice(this.lsd_num + 1, 0, 0); 
			// now the length of the array has increased,
	// but we can play it safe and reduce it if we suddenly exceed maxlen:
	if (this._digits.length > this.maxlen) {
	// cut off the excess
				this._digits.length = this.maxlen; 
				console.log("Overflow in shiftBaseLeft cut off extra digit");
			} else {
				this.lsd_num++;
			}
			return this;
		}
	
		// A simplified method for adding a "single-digit" TLNum (in the BASE database).
		// Adds to this the value of one digit "digit" (0 <= digit < BASE).
		addSingleDigit(digit) {
			let buffer_short = new TBuffer([this.first(), digit]);
			while (buffer_short.notEmpty()) {
				this.currentVal(buffer_short.rem());
				if (buffer_short.notEmpty()) {
					buffer_short.PushSum(this.next());
					if (this.isFull()) {
						console.log("Overflow in addSingleDigit");
						break;
					}
	}
	}
	// updating lsd_num if it has grown
			if (this._current > this.lsd_num) {
				this.lsd(this._current);
			}
	// going back to the beginning
			this.first();
			return this;
		}
	
		// Binary coefficient search function (guess),
	// so that LNum*guess <= partial, and LNum*(guess+1) > partial.
		// Returns a guess integer in the range [0..MAX_DIGIT_VALUE].
		// partial and divisor are both TLNum.
		findGuessByBinarySearch(partial, divisor) {
			let left = 0;
			let right = MAX_DIGIT_VALUE;
			let best = 0;
	
			while (left <= right) {
				let mid = Math.floor((left + right) / 2);
	
				// creating a temporary TLNum = divisor * mid
				let midVal = new TLNum({digits: divisor.digits().slice()});
				midVal.mul( new TLNum({digits:[mid], negative:false}) );
	
				// comparing partial and midVal
				let cmp = partial.compare(midVal);
				if (cmp >= 0) {
					// so partial >= midVal, we can raise the bid
					best = mid;
					left = mid + 1;
				} else {
					// partial < midVal
					right = mid - 1;
				}
			}
			return best;
		}
	
		// Main division method: this div LNum => (this = quotient), this.rem = remainder
		div(LNum) {
	// 1. Check division by zero
	if (LNum.lsd() === 0 && LNum._digits[0] === 0) {
				console.log("Error: division by zero");
				return this; 
			}
	
			// 2. Sign processing
			let finalSign = (this.negative !== LNum.negative);  // XOR
			// Let's keep the original signs:
			let oldNegA = this.negative;
			let oldNegB = LNum.negative;
			// Let's remove the signs to work with absolute values
			this.negative = false;
			LNum.negative = false;
	
			// 3. Module comparison: if this < LNum, then result = 0, remainder = this
			if (this.compare(LNum) < 0) {
				// quotient = 0
	let zeroArr = ZRO_ARRAY(this.maxlen); 
				this.digits(zeroArr);
				this.lsd(0); 
				// Remainder = the original value (modulo), but it is more convenient to copy:
				let remainder = new TLNum({
					digits: ZRO_ARRAY(this.maxlen), 
					n: false
				});
				remainder.digits(this._digits.slice()); // taking a copy
				remainder.lsd(this.lsd_num);
				this.rem = remainder;
				// Restore the signs (the remainder has the same sign as the divisible)
	this.negative = false; // quotient = 0 — we can leave it unsigned
				remainder.negative = oldNegA; // signed remainder as a divisible
				// Restoring the LNum sign (just in case)
	LNum.negative = oldNegB;
				return this;
			}
	
			// 4. Let's organize a top-down loop
	// Create a TLNum for the quotient (we will write the result there):
			let quotient = new TLNum({
				digits: ZRO_ARRAY(this.maxlen),
				n: false
			});
	
			// Creating a partial (intermediate remainder), starting from 0
	let partial = new TLNum({
				digits: ZRO_ARRAY(this.maxlen),
				n: false
			});
	
			/// we go by digits from the highest (lsd_num) to the lowest (0)
	// Remember that lsd_num is the "rightmost non—zero index". 
			// But the “senior” in our case is just lsd_num,
			// and we go up to 0, including.
			for (let i = this.lsd_num; i >= 0; i--) {
	
				// step a) partial = partial * BASE
				partial.shiftBaseLeft();
	
				// step b) partial = partial + the current digit of this._digits[i]
				// (exactly i, because i goes from the oldest to 0)
	partial.addSingleDigit(this._digits[i]);
	
				// step c) find through binary search how many times divisor (LNum)
	// is "placed" in partial
				let g = this.findGuessByBinarySearch(partial, LNum);
	
				// step d) quotient._digits[i] = g
				quotient._digits[i] = g;
	
				// step e) partial = partial - LNum*g
				if (g > 0) {
					// building LNum*g
					let mulVal = new TLNum({digits: LNum.digits().slice(), n: false});
					mulVal.mul( new TLNum({digits: [g], n: false}) );
	
					partial.sub(mulVal); // what doesn't fit remains
	}
	}
	
			// updating the lsd_num of the quotient (quotient)
	quotient.auto_len();
	
			// 5. Now the quotient contains the full quotient, and the partial contains the remainder.
			// Let's move them to this and this.rem
	
			// Copy the quotient numbers to this
			this.digits( quotient.digits().slice() );
			this.lsd( quotient.lsd() );
			// And we'll save the rest in this.rem
	let remainder = new TLNum({
				digits: partial.digits().slice(),
				n: false
			});
			remainder.lsd( partial.lsd() );
			this.rem = remainder;
	
			// 6. Restoring the signs
			// - the quotient gets finalSign if it is not zero
	// - the remainder gets the original sign of the oldNegA divisible
			// (this is often the case in mathematics).
			// Check if the quotient is zero:
	if (this.lsd() === 0 && this._digits[0] === 0) {
				// quotient == 0 => the sign can be left false
				this.negative = false;
			} else {
				this.negative = finalSign;
			}
	// remainder — if it is not zero, let it have a divisible sign
			if (!(this.rem.lsd() === 0 && this.rem._digits[0] === 0)) {
				this.rem.negative = oldNegA;
			}
	
			// 7. Restore the sign of LNum (so as not to "spoil" the passed argument)
	LNum.negative = oldNegB;
	
			return this;
		}


}